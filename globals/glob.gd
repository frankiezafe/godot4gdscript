extends Node


const cam_scn:PackedScene = preload("res://objects/cam_arm.tscn")
const PLAYER_START:String = "player_start"

var current_scene = null

func search_player_start(parent:Node) -> Node3D:
	for n in parent.get_children():
		if n is Node3D and n.name == PLAYER_START:
			return n
		var rec:Node3D = search_player_start(n)
		if rec != null:
			return rec
	return null

func add_camera():
	var c = cam_scn.instantiate()
	current_scene.add_child(c)
	var ps:Node3D = search_player_start(current_scene)
	if ps:
		print(ps)
		c.global_position = ps.global_position
	c.set_current()

func _ready():
	var root = get_tree().root
	current_scene = root.get_child(root.get_child_count() - 1)
	# making sure a camera is available
	add_camera()

func goto_scene(path):
	call_deferred("_deferred_goto_scene", path)

func _deferred_goto_scene(path):
	current_scene.free()
	# Load the new scene.
	var s = ResourceLoader.load(path)
	# Instance the new scene.
	current_scene = s.instantiate()
	# Add it to the active scene, as child of root.
	get_tree().root.add_child(current_scene)
	# Optionally, to make it compatible with the SceneTree.change_scene_to_file() API.
	get_tree().current_scene = current_scene
	# spawning a camera
	add_camera()
