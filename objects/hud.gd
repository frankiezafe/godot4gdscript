extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$main/main_panel/list/fps.text = str(Engine.get_frames_per_second()) + " fps"
	var nc:int = get_tree().get_node_count()
	var oc:String = str(nc) + " object"
	if nc > 1:
		oc += "s"
	$main/main_panel/list/objects.text = oc
