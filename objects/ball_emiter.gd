extends Node3D

@export var maximum_balls:int = 100
@export var emition_radius:float = 0.5
@export var emition_rate:float = 1
@export var lifetime_min:float = 4
@export var lifetime_max:float = 6

@onready var ball_scn = load("res://objects/ball.tscn")

var balls:Array[Ball] = []
var emit_delta:float = 0

func _ready():
	if lifetime_min > lifetime_max:
		lifetime_min = lifetime_max

func _process(delta):
	emit_delta += delta * emition_rate
	while emit_delta > 1:
		emit_delta -= 1
		if get_children().size() < maximum_balls:
			var b = ball_scn.instantiate()
			b.lifetime = randf_range(lifetime_min, lifetime_max)
			add_child(b)
			b.global_transform = global_transform
			b.global_transform.origin += Vector3(
				randf_range(-emition_radius, emition_radius),
				randf_range(-emition_radius, emition_radius),
				randf_range(-emition_radius, emition_radius)
			)
