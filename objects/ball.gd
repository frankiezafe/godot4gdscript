class_name Ball
extends RigidBody3D

@export var material:Material
@export var albedo0:Color
@export var albedo1:Color
@export_range(0,1) var bounce:float = 0.3
@export var lifetime:float = 3

var render_mat:Material = null
var physic_mat:PhysicsMaterial = null
var time:float = 0

func isBall() -> bool:
	return true

func init_mats() -> void:
	# render material mamagament
	if material == null:
		render_mat = $display.mesh.surface_get_material(0).duplicate()
	else:
		render_mat = material.duplicate()
	$display.material_override = render_mat
	apply_albedo()
	# physics managent
	physic_mat = physics_material_override.duplicate()
	physic_mat.bounce = bounce
	physics_material_override = physic_mat

func _ready():
	init_mats()
	apply_scale()
	contact_monitor = true
	max_contacts_reported = 1

func apply_albedo() -> void:
	if render_mat == null:
		return
	if render_mat is ShaderMaterial:
		render_mat.set_shader_parameter('albedo0', albedo0)
		render_mat.set_shader_parameter('albedo1', albedo1)
	elif render_mat is Material:
		render_mat.albedo_color = albedo1

func apply_scale() -> void:
	if time < 1:
		$display.scale = Vector3.ONE * time
	elif time >= lifetime - 0.5:
		$display.scale = Vector3.ONE * max(0, min(1, (lifetime-time)*2))
	else:
		$display.scale = Vector3.ONE

func _process(delta):
	time += delta
	apply_scale()
	if time >= lifetime:
		queue_free()

func _on_body_entered(body):
	
	if !body.get_parent() is Node3D:
		return
	albedo1 = Color.from_hsv( randf(),1,1 )
	apply_albedo()
