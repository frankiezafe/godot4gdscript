@tool
extends Node3D

@export_category("Camera")
@export_range(1,180) var fov:float = 50 : 
	set(value):
		fov = float(value)
		$pivot/cam.fov = fov

@export_range(0,100) var arm:float = 15 : 
	set(value):
		arm = float(value)
		$pivot/cam.position.z = arm

@export_category("Motion")
@export_range(-180,180) var pitch_start:float = 10 : 
	set(value):
		pitch_start = float(value)
		$pivot.rotation.x = -pitch_start / 180 * PI
@export_range(-180,180) var pitch_min:float = 10
@export_range(-180,180) var pitch_max:float = 80
@export var rotation_speed:Vector2 = Vector2(1,1)
@export var rotation_damping:Vector2 = Vector2(5,5)

var mouse_down:bool = false
@onready var yaw:float = rotation.y
@onready var pitch:float

func set_current():
	$pivot/cam.current = true

func _ready():
	pitch_start = pitch_start
	pitch = pitch_start

func _process(delta):
	var _yaw:float = yaw / 180 * PI
	var _pitch:float = -pitch / 180 * PI
	rotation.y += ( _yaw - rotation.y) * delta * rotation_damping.y
	$pivot.rotation.x += ( _pitch - $pivot.rotation.x) * delta * rotation_damping.x

func _input(event):
	if event is InputEventMouseButton and event.button_index == 1:
		mouse_down = event.is_pressed()
	if mouse_down and event is InputEventMouseMotion:
		yaw += event.relative.x * rotation_speed.y
		pitch += event.relative.y * rotation_speed.x
		pitch = min( pitch_max, max( pitch_min, pitch ) )
